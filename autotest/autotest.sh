#!/bin/bash

# This script executes the program with the given input values and shows the results.

PROG="s_solver"
T_PROG="auto_test"
COUNT=$(find . -name "*.inp" | wc -l)  # count the input files.
SOURCE=$(dirname "${BASH_SOURCE[0]}")

mkdir "$SOURCE/results"
for (( II=1; II<=$COUNT; II++ ))
do
  echo -e "\n\nDoing Test #$II..."
  cp "$SOURCE/$II.inp" "$SOURCE/../schrodinger.inp"
  "$SOURCE/../$PROG" $1
  mv "$SOURCE/../discrpot.dat" "$SOURCE/results/discrpot_$II.dat"
  mv "$SOURCE/../energies.dat" "$SOURCE/results/energies_$II.dat"
  mv "$SOURCE/../wfuncs.dat" "$SOURCE/results/wfuncs_$II.dat"
  mv "$SOURCE/../ewfuncs.dat" "$SOURCE/results/ewfuncs_$II.dat"
  rm "$SOURCE/../schrodinger.inp"
done
echo "Test calculated values"
"$SOURCE/../$T_PROG" $1

echo "Test completed. Plotting the graphs..."
cd $SOURCE
gnuplot ./plot.gp
echo "Done!"
