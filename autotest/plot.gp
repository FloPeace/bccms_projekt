# A simple Gnuplot script to plot the calculated results.

set terminal jpeg size 900, 900
set xlabel ("x [Bohr]")
set ylabel ("Energy [Ha]")
set output "./results/plot_1.jpg"
plot [-5:5][-0.25:2.75] for [col=2:5] "./results/ewfuncs_1.dat" using 1:col with lines, "./results/discrpot_1.dat" using 1:2 with lines
set output "./results/plot_2.jpg"
plot [-2:2][-10.1:1] for [col=2:4] "./results/ewfuncs_2.dat" using 1:col with lines, "./results/discrpot_2.dat" using 1:2 with lines
set output "./results/plot_3.jpg"
plot [-5:5][0:5] for [col=2:10] "./results/ewfuncs_3.dat" using 1:col with lines, "./results/discrpot_3.dat" using 1:2 with lines
set output "./results/plot_4.jpg"
plot [-12:12][-1.6:2.7] for [col=2:17] "./results/ewfuncs_4.dat" using 1:col with lines, "./results/discrpot_4.dat" using 1:2 with lines