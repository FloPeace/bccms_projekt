FC = gfortran
FCOPTS = -llapack -pedantic -Wall -fbounds-check
LN = $(FC)
LNOPTS = -llapack
BORDERTEST=""

OBJS = accuracy.o io.o math.o seq_solver.o s_solver.o
A_OBJS = accuracy.o io.o autotest.o

.PHONY: all
all: s_solver auto_test

s_solver: $(OBJS)
	$(LN) -o $@ $^ $(LNOPTS)

auto_test: $(A_OBJS)
	$(LN) -o $@ $^

%.o: src/%.f90
	$(FC) -c $< $(FCOPTS)

accuracy.o:

seq_solver.o: accuracy.o

io.o: accuracy.o

math.o: accuracy.o

autotest.o: accuracy.o io.o

s_solver.o: accuracy.o io.o math.o seq_solver.o


.PHONY: clean realclean test

clean:
	rm -f *.o *.mod *.dat && rm -fr ./autotest/results

realclean: clean
	rm -f s_solver auto_test

test:
	./autotest/autotest.sh $(BORDERTEST)
