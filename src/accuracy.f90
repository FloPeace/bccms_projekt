!> global accuracy options
module accuracy
  implicit none

  !> accuracy for real
  integer, parameter :: dp = kind(0.0d0)

end module accuracy