  !> input and output routines

module io
  use accuracy
  implicit none

  contains

  !> Reads the information from the data file
  !!
  !! \param inputfile name of the data file
  !! \param mass mass of the particle
  !! \param xmin lower limit of the wavefunktion
  !! \param xmax upper limit of the wavefunktion
  !! \param nn number of iterations
  !! \param typ interpolation typ
  !! \param vv are fixpoints of the potential
  !! \param ev lower and upper limit of eigenvalues that will saved

  subroutine readinput(inputfile, mass, xmin, xmax, nn, typ, vv, ev)
    character(len=*), intent(in) :: inputfile
    integer, intent(out) :: nn, ev (2)
    character(7), intent(out) :: typ
    real(dp), intent(out) :: mass, xmin, xmax
    real(dp), allocatable, intent(out) :: vv(:,:)
    
    !help parameter 
    integer :: points, ii, open_status

    ! open the file
    open(12, file=inputfile, status="old", iostat=open_status)
    if (open_status /= 0) then
      write (*, *) "Error: readinput: File cannot be opened!"
      stop
    end if
    
    !read the informations
    read(12,*) mass
    read(12,*) xmin, xmax, nn
    read(12,*) ev(:)
    read(12,*) typ
    read(12,*) points

    ! read the interpolation points
    allocate(vv(points, 2))

    do ii= 1, points
       read(12,*) vv(ii,:)
    end do

    close(12)
  end subroutine readinput
  
  
  !> Writes the calculated results to files.
  !!
  !! \param xx The x values the other values refer to.
  !! \param Vy The potential energies.
  !! \param En The calculated Eigenvalues (The energies).
  !! \param Wf The Wavefunctions; This is a 2-dim. array (x, E).
  !! \param EW_from The first Eigenvalue and Wavefunction to write.
  !! \param EW_to The last Eigenvalue and Wavefunction to write.
  subroutine write_result (xx, Vy, En, Wf, EW_from, EW_to)
    real(dp), intent (in) :: xx (:), Vy (:), En (:) , Wf (:,:)
    integer, intent (in) :: EW_from, EW_to
    
    integer :: ii
    character (len=256) :: write_string
    
    !Check for validity of given data
    if ((EW_from >= EW_to) .or. (EW_from < 0) .or. (EW_to < 0) .or. &
    & (size (xx) /= size (Vy)) .or. (size (En) /= size (Wf, 2)) .or. &
    & (size (xx) /= size (Wf, 2)) .or. (EW_to > size (En))) then
      write (*, *) "Error: write_result: The given data wasn't valid!"
      stop
    end if
    
    !Open the nessessary files.
    open (13, file="discrpot.dat", status="replace", form="formatted")
    open (14, file="energies.dat", status="replace", form="formatted")
    open (15, file="wfuncs.dat", status="replace", form="formatted")
    open (16, file="ewfuncs.dat", status="replace", form="formatted")
    
    !Write the potential
    do ii=1, size (Vy)
      write (13, "(ES23.15, 1X, ES23.15)") xx (ii), Vy (ii)
    end do
    close (13)
    
    !Write the eigenvalues (energies).
    write (14, "(ES23.15)") En (EW_from:EW_to)
    close (14)
    
    !Create the write string
    write (write_string, "(A, I0, A)") "(", (EW_to - EW_from + 2), "(ES23.15, X))"
    
    !Write the wavefunctions
    do ii=1, size (xx)
      write (15, write_string) xx (ii), Wf (ii, EW_from:EW_to)
      write (16, write_string) xx (ii), Wf (ii, EW_from:EW_to) + En (EW_from:EW_to)
    end do
    close (15)
    close (16)
  end subroutine write_result
  
  !> Read the eigenvalues of the solution
  !!
  !! \param fname Name of File with the solution
  !! \param ew vector with the solution
  subroutine readsolution(fname, ew)
    character(len=*), intent(in) :: fname
    real(dp), intent(out) :: ew(:)

    integer, parameter :: fd = 42

    open(fd, file=fname, status="old", action="read", form="formatted")
    read(fd, *) ew
    close(fd)

  end subroutine readsolution

end module io
