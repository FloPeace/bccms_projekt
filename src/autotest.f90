!> Tests two result files on compatibility.
program autotest
  use accuracy
  use io
  implicit none
  
  character(28), dimension(4), parameter :: origname = (/"autotest/energies_1.dat.orig", & 
  &"autotest/energies_2.dat.orig", "autotest/energies_3.dat.orig", "autotest/energies_4.dat.orig"/)
  character(31), dimension(4), parameter :: newname = (/"autotest/results/energies_1.dat", &
  &"autotest/results/energies_2.dat", "autotest/results/energies_3.dat", "autotest/results/energies_4.dat"/)
  character(len=*), parameter :: testok = "TEST PASSED"
  character(len=*), parameter :: testfailed = "!!! TEST FAILED !!!"
  real(dp), parameter :: errortol = 1e-10_dp
  integer :: ii
  
  real(dp), dimension(15,4) :: orig(15,4), new(15,4)

  do ii=1,4
    call readsolution(origname(ii), orig(:,ii))
    call readsolution(newname(ii), new(:,ii))
    if (maxval(abs(new(:,ii) - orig(:,ii))) >= errortol) then
      write(*, "(A,1X,A)") testfailed, "Data shapes are different"
    elseif (maxval(abs(new(:,ii) - orig(:,ii))) <= errortol) then
      write(*,"(A,I2,1X,A)") "test #", ii, testok
    else
      write(*,"(A,1X,A)") testfailed, "Another Problem"
    end if
  end do


end program autotest
