!> \mainpage Mainprogram to solve the schrodinger equation
!!
!!first the programm import the data from schrodinger.inp
!!then it covert data and call the solve algorithm
!!and then quest for a controll option
!!at least the program output the data

program s_solver
  use accuracy
  use io
  use math
  use seq_solver
  implicit none

  character(*), parameter :: inputfile = "schrodinger.inp"

  real(dp), allocatable :: vp(:,:), xx(:), VV(:), DD(:), WF(:,:), WF_2(:,:), DD_2(:), VV_2(:), xx_2(:)
  real(dp) :: mass, xmin, xmax, delta, xmin_2, xmax_2
  integer :: nn, ii, INFO, ev (2), INFO_2
  character(7) :: typ, quest
  
  !The coefficients for a polynominal interpolation
  real(dp), allocatable :: polynominal (:)
  
  !Check whether an additional test for the wavefunction should be done.
  if (command_argument_count () > 0) then
    call get_command_argument (1, quest)
  else
    quest = "N"
  end if
  
  !Import the data from schrodinger.inp
  write(*, "(3A)") "read input file '", inputfile, "'"
  call readinput(inputfile, mass, xmin, xmax, nn, typ, vp, ev)
  write(*,*) "read complete"

  !calculate the step size
  delta = (xmax - xmin)/(nn-1)

  
  !create all x values for V(x) and WF(x)
  allocate(xx(nn))
  do ii = 0, (nn-1)
     xx(ii+1) = (ii)*delta + xmin
  end do

  !allocate the Potential V(x)
  allocate(VV(nn))

  write(*,*) "Data conversion complete"
  write(*,*) "interpolation typ:  ", typ

  !interpolate the potential linear or polynominal
  if(typ == "linear ")then
     do ii = 1, nn
        call interpolate_linear(vp (:,1), vp (:,2), xx(ii), VV(ii))
     end do
  elseif(typ == "polynom")then
     call create_polynomial (vp (:,1), vp (:,2), polynominal)
     do ii = 1, nn
        call calc_polynomial (polynominal, xx(ii), VV(ii))
     end do
  else
     write(*,*) 'wrong interpolation type, please correct the type'
     stop
  end if

  !calculate the eigenvalues
  write(*,*) 'solve the eigenvalue matrix'
  call m_solver(mass, nn, delta, VV, DD, WF, INFO)
  !out : DD (eigenvalues), WF (weavefunctions) and INFO (error info)

  !display error informations
  if(INFO == 0)then
     write(*,*) 'matrix successfully solved without errors'
  elseif(INFO < 0)then
     write(*,*) 'error, illegal value for matrix element', INFO
     stop
  elseif(INFO > 0)then
     write(*,*) 'error, not convergent value for matrix element', INFO
     stop
  end if

  !Maybe test accuracy of the wavefunction by matching the WF with another with double borders
  !this is just optional
  if(maxval(vp(:,2)) /= minval(vp(:,2)))then
    if(quest == "Y")then
      xmin_2 = xmin - (xmax - xmin)/2
      xmax_2 = xmax + (xmax - xmin)/2
      allocate(xx_2(2*nn))
      allocate(VV_2(2*nn))
      do ii = 0, (2*nn+1)
        xx_2(ii+1) = (ii)*delta + xmin_2
      end do

      !make Potential V_2(x)
      if(typ == "linear ")then
        do ii = 1, (2*nn)
          call interpolate_linear(vp (:,1), vp (:,2), xx_2(ii), VV_2(ii))
        end do
      elseif(typ == "polynom")then
        call create_polynomial (vp (:,1), vp (:,2), polynominal)
        do ii = 1, 2*nn
          call calc_polynomial (polynominal, xx_2(ii), VV_2(ii))
        end do
      else
        write(*,*) 'wrong interpolation type, please correct the typ'
        stop
      end if

      !great border matrix solve
      write(*,*) 'solve the great border eigenvalue matrix'
      call m_solver(mass, (2*nn), delta, VV_2, DD_2, WF_2, INFO_2)

      !display error information
      if(INFO_2 == 0)then
         write(*,*) 'matrix successfully solved without errors'
      elseif(INFO_2 < 0)then
         write(*,*) 'error, illegal value for matrix element', INFO
         stop
      elseif(INFO_2 > 0)then
         write(*,*) 'error, not convergent value for matrix element', INFO
         stop
      end if
      write(*,*) 'greatest deviation from low-border WF:', maxval(WF_2((nn/2):nn + (nn/2) - 1,(nn/2):nn + (nn/2) - 1) - WF)
    end if
  end if

  !Write the results to the files.
  call write_result (xx, VV, DD, WF, ev (1), ev (2))
end program s_solver
