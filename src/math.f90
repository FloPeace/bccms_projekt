!> Some mathemathical functions.

module math
  use accuracy
  implicit none
  
  contains
  
  
  !> Creates a polynomial of degree (n - 1) of n points.
  !! \details The problem can be turned into a linear equation system
  !!   A*c = y, where A is a matrix of the powers of the x values, c is a
  !!   vector of the polynomial coefficients and y is a vector of the y
  !!   values.
  !! \param px The x values of the points.
  !! \param py The y values of the points.
  !! \param coeffs The coefficients of the resulting polynomial.
  subroutine create_polynomial (px, py, coeffs)
    real(dp), intent (in) :: px (:), py (:)
    real(dp), allocatable, intent (out) :: coeffs (:)
    
    real(dp), allocatable :: poly_mat (:, :), pivot (:)
    integer :: cx, cy, return_value
    
    if (size (px) == size (py)) then
      allocate (coeffs (size (px)))
      allocate (poly_mat (size (px), size (py)))
      coeffs = py
      
      !Fill the polynomial matrix
      do cx = 1, size (px)
        do cy = 1, size (py)
          if (cx == 1) then
            poly_mat (cy, cx) = 1
          else
            poly_mat (cy, cx) = px (cy) ** (cx - 1)
          end if
        end do
      end do
      
      !Solve the LES with LAPACK
      allocate (pivot (size (px)))
      call DGESV (size (px), 1, poly_mat, size (px), pivot, coeffs, size (coeffs), return_value)
    else
      write (*, *) "Error: create_polynomial: You need as many x values as y values."
      stop
    end if
  end subroutine create_polynomial
  
  
  !> Calculate a polynomial P(x) with coefficients given.
  !! \param polynomial A vector of the coefficients of the polynomial.
  !! \param x The x value to calculate P(x) for.
  !! \param value The result of P(x).
  subroutine calc_polynomial (polynomial, x, value)
    real(dp), intent (in) :: polynomial (:)
    real(dp), intent (in) :: x
    real(dp), intent (out) :: value
    
    integer :: ii
    
    !Calculate the polynomial for x
    !Set the first coefficient of the polynomial for x^0
    value = polynomial (1)
    do ii = 1, size (polynomial) - 1
      value = value + polynomial (ii + 1) * (x ** ii)
    end do
  end subroutine calc_polynomial
  
  
  !> Do a linear interpolation and return the approximated value at x.
  !! \details The two points on the left and the right side of the given
  !!   x value are picked and a function of the form P(x) = mx + b is created.
  !! \param px The x values of the points.
  !! \param py The y values of the points.
  !! \param x The place to calculate the value for.
  !! \param value The value of the interpolation at x.
  !! \note This function can only work if px is ordered from small to big.
  subroutine interpolate_linear (px, py, x, value)
    real(dp), intent (in) :: px (:), py (:)
    real(dp), intent (in) :: x
    real(dp), intent (out) :: value
    
    !The index of the two closest x points from px that surround x left and right
    integer :: l_closest, r_closest
    integer :: ii
    real(dp) :: m, b
    
    !Check for validity of given data
    if (size (px) /= size (py)) then
      write (*, *) "Error: interpolate_linear: Wrong data input!"
      stop
    end if
    
    !Search for the surrounding values
    l_closest = 1
    r_closest = 1

    !Search for the biggest value that is smaller then x.
    do ii = 1, size (px) - 1
      if (px (ii) > x) then
        exit
      else
        l_closest = ii
      end if
    end do
    
    r_closest = l_closest + 1
    
    !Calculate the linear function y(x) = mx + b
    m = (py (r_closest) - py (l_closest)) / (px (r_closest) - px (l_closest))
    b = py (l_closest) - m * px (l_closest)
    value = m * x + b
  end subroutine interpolate_linear
  
  
  !> Do a polynomial interpolation and return the approximated value at x.
  !!
  !! \param px The x values of the points.
  !! \param py The y values of the points.
  !! \param x The place to calculate the value for.
  !! \param value The value of the interpolation at x.
  !! \note It's probably better to use create_polynomial and calc_polynomial manually,
  !!   because this subroutine calls create_polynomial again every time you call it.
  subroutine interpolate_polynomial (px, py, x, value)
    real(dp), intent (in) :: px (:), py (:)
    real(dp), intent (in) :: x
    real(dp), intent (out) :: value
    
    !The coefficients for the interpolation polynomial
    real(dp), allocatable :: polynomial (:)
    
    call create_polynomial (px, py, polynomial)
    call calc_polynomial (polynomial, x, value)
  end subroutine interpolate_polynomial
end module math