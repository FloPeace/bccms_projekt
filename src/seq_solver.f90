  !> Contains routines for solving matrix eigenvalue problems

module seq_solver
  use accuracy
  implicit none

  contains

  
  !> Calculate the wavefunctions with discretisation of the srodinger equation
  ! over solving the matrix
  !!
  !! \param mass the mass of the particle in the system
  !! \param nn number of x values, also order of the matrix
  !! \param delta the step size
  !! \param VV vector with V(x) values
  !! \param DD vector with eigenvalues
  !! \param WF vactor with weavefunctions WF(number of wf's : Y(x) values)
  !! \param INFO is a control parameter cfor solve algorithm

  subroutine m_solver(mass, nn, delta, VV, DD, WF, INFO)
    real(dp), intent(in) :: mass, delta, VV(:)
    integer, intent(in) :: nn
    real(dp) :: aa
    real(dp), allocatable :: EE(:), WORK(:,:)
    integer, intent(out) :: INFO
    real(dp), allocatable, intent(out) :: DD(:), WF(:,:)
    
    !nn is the order of the matrix
    !allocate the matrix DD and EE
    allocate(DD(nn))
    allocate(EE(nn))
    allocate(WF(nn, nn))
    allocate(WORK(2*nn, 2*nn))

    !calculate factor a
    aa = (1 / (mass * delta * delta))

    !write diagonal elements in Vector DD (in output the eigenvalues ascending)
    DD(:) = aa + VV(:)

    !write n-1 offdiagonal elements in Vector EE(in output destroyed)
    EE(:) = - 0.5 * aa
    EE(nn) = 0 !Destroy the last element (if nessessary)
    
    !solve with algorithm from LAPACK
    call DSTEV ('V', nn, DD, EE, WF, nn, WORK, INFO)

  end subroutine m_solver
end module seq_solver