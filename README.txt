This program calculates the 1-dimensional Schrödinger-equation. It reads the data from 'schrodinger.inp' and writes
the results to:
  discrpot.dat	-	The discretised potential V(x)
  energies.dat	-	The eigenvalues (energies) of the problem
  wfuncs.dat	-	The x-values in the first column, the requested wavefunctions in columns [2:(n+1)]
  ewfuncs.dat	-	The sum of the wavefunctions and the energies.

If you want to know, how the input file 'schrodinger.inp' has to look like, take a look at the examples in /autotest/{1-4}.inp.

You can compile the program using:
  $ make && make clean
Compile the Test functions:
  $ make all
Do the autotest with the given four examples by typing:
  $ make test
Delete everything:
  $ make realclean
  
If the tests one to four calculated, the values will be compare with the original values.  
You can see the results of the autotest in autotest/results.
After you wrote your personal input file, just call it 'schrodinger.inp', place it in the same folder as the file
's_solver' and execute:
  $ ./s_solver

This is only an approximation. Its accuracy depends on the chosen borders. There is an additional test that
compares your result with the calculation with the double border range, but it needs more calculation time. If you
want to do so just type:
  $ make test BORDERTEST=Y
  
See the documentation in /doc for a detailed description of the functions.

Comiling requirements:
  This software is only tested on Linux/Unix platforms. It might not work properly on others. Make sure you installed the
  'gfortran' fortran compiler. You will also need:
    - Gnuplot
    - lapack-devel
    - blas-devel

Good luck!
  Florian Littau
  Daniel Steinhauer
